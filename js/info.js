﻿SKA = SKA || {};

SKA.InfoScreen = me.ScreenObject.extend(
{
    init: function (image, nextState)
    {
        this.parent(true, true);
        this.background = null;
        this.image = image;
        this.nextState = nextState;
    },

    /**	
        *  action to perform on state change
        */
    onResetEvent: function ()
    {

        this.background = me.loader.getImage(this.image);

    },

    update: function ()
    {
        this.parent();
        if (me.input.isKeyPressed("enter") || me.input.isKeyPressed("escape"))
            me.state.change(this.nextState);

        return true;
    },

    draw: function (context)
    {
        context.drawImage(this.background, 0, 0, me.video.getWidth(), me.video.getHeight());
    }
});