SKA = SKA || {};

SKA.resources = [

	/* Graphics. 
	 * @example
	 * {name: "example", type:"image", src: "res/img/example.png"},
	 */
	
    //Screen elements
    { name: "title_bg",     type: "image", src: "res/img/title_bg.png" },
    { name: "play",         type: "image", src: "res/img/play.png" },
    { name: "credits",      type: "image", src: "res/img/credits.png" },
    { name: "title_select", type: "image", src: "res/img/title_select.png" },
    { name: "credits_bg",   type: "image", src: "res/img/credits_bg.png" },

	/* Maps. 
	 * @example
	 * {name: "example01", type: "tmx", src: "res/map/example01.tmx"},
 	 */

	/* Background music. 
	 * @example
	 * {name: "example_bgm", type: "audio", src: "res/sfx/", channel : 1},
	 */	
	
	/* Sound effects. 
	 * @example
	 * {name: "example_sfx", type: "audio", src: "res/sfx/", channel : 2}
	 */
];
