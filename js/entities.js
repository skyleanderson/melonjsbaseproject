SKA = SKA || {};

SKA.PlayerEntity = me.ObjectEntity.extend(
{

    init: function (x, y, settings)
    {
        //timing
        SKA.lastTime = new Date().getTime();
        SKA.currTime = null;
        SKA.millis = 0;

        // call the constructor
        settings.image = "";
        settings.spritewidth = 185;
        settings.spriteheight = 131;
        y -= 48;
        this.parent(x, y, settings);
        //this.renderable.resize(2);

        // set the walking & jumping speed
        //set to large number since it will be updated by tweener... might not need it at all
        this.setVelocity(100, 100);

        // adjust the bounding box
        //		this.updateColRect(8, 16, 0, 32);

        // set the display to follow our position on both axis
        //this.jumping = false;
        //this.falling = false;
        //this.gravity = 0;
        //this.renderable.alwaysUpdate = true;

        //add this object to the global namespace so others can reference it
        SKA.player = null;
        SKA.player = this;

        me.game.sort();

        this.renderable.addAnimation("stand", [0]);
        this.renderable.addAnimation("walk", [1]);
    },

    setAnimation: function (animation, restart)
    {
        if (restart)
        {
            //just set animation, this will restart it
            this.renderable.setCurrentAnimation(animation, animation);
            this.renderable.setAnimationFrame();
        } else
        {
            //only set if it is a different animation
            if (!this.renderable.isCurrentAnimation(animation))
            {
                this.renderable.setCurrentAnimation(animation, animation);
                this.renderable.setAnimationFrame();
            }
        }
    },

    update: function ()
    {
        //timing
        SKA.currTime = new Date().getTime();
        SKA.millis = SKA.currTime - SKA.lastTime;
        if (SKA.millis > 500) SKA.millis = 16;

        //input
        if (me.input.isKeyPressed('top'))
        {
           //do something
        }

        //set animation
        if (this.vel.x > 0)
        {
            //set to moving
            this.setAnimation('walk');
        } 

        this.parent(this);
        SKA.lastTime = SKA.currTime;
        return true;

    },

    draw: function (context)
    {
        this.parent(context);
    }
});